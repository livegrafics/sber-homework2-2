package Task3;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        char[][] a = new char[n][n];
        int x = scanner.nextInt();
        int y = scanner.nextInt();
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                a[i][j] = '0';
            }
        }
        a[y][x] = 'К';
        if (y - 2 >= 0 && x - 1 >= 0) a[y - 2][x - 1] = 'Х';
        if (y - 2 >= 0 && x + 1 <= n - 1) a[y - 2][x + 1] = 'Х';
        if (y - 1 >= 0 && x - 2 >= 0) a[y - 1][x - 2] = 'Х';
        if (y - 1 >= 0 && x + 2 <= n - 1) a[y - 1][x + 2] = 'Х';

        if (y + 2 <= n - 1 && x - 1 >= 0) a[y + 2][x - 1] = 'Х';
        if (y + 2 <= n - 1 && x + 1 <= n - 1) a[y + 2][x + 1] = 'Х';
        if (y + 1 <= n - 1 && x - 2 >= 0) a[y + 1][x - 2] = 'Х';
        if (y + 1 <= n - 1 && x + 2 <= n - 1) a[y + 1][x + 2] = 'Х';

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(a[i][j]);
                if (j != (n - 1)) System.out.print(" ");
            }
            System.out.println();
        }
    }
}
