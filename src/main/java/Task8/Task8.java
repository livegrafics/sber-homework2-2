package Task8;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int number = 0;
        number = countNumber(n, 0);
        System.out.println(number);
    }

    public static int countNumber(int n, int number) {
        if (n < 10) {
            return number + n;
        } else {
            number = number + countNumber(n / 10, n % 10);
        }
        return number;
    }
}
