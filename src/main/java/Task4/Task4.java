package Task4;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[][] a = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                a[i][j] = scanner.nextInt();
            }
        }
        int p = scanner.nextInt();
        int row = 0;
        int column = 0;
        boolean isFind = false;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (a[i][j] == p) {
                    row = i;
                    column = j;
                    isFind = true;
                    break;
                }
            }
            if (isFind) break;
        }
        int[][] result = new int[n - 1][n - 1];
        int ii = 0;
        int i = 0;
        int j = 0;
        int jj = 0;
        while (i < n) {
            if (i != row) {
                jj = 0;
                while (j < n) {
                    if (j != column) {
                        result[ii][jj] = a[i][j];
                        jj++;
                    }
                    j++;
                }
                j = 0;
                jj = 0;
                ii++;
            }
            i++;
        }
        for (i = 0; i < n - 1; i++) {
            for (j = 0; j < n - 1; j++) {
                System.out.print(result[i][j]);
                if (j != (n - 2)) {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
