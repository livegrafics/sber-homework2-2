package Task6;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[][] arr = new int[8][4];
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[0].length; j++) {
                arr[i][j] = scanner.nextInt();
            }
        }
        boolean isBetter = true;
        for (int j = 0; j < arr[0].length; j++) {
            int summ = 0;
            for (int i = 1; i < arr.length; i++) {
                summ = summ + arr[i][j];
            }
            if (summ > arr[0][j]) {
                isBetter = false;
                break;
            }
        }
        if (isBetter) {
            System.out.println("Отлично");
        } else {
            System.out.println("Нужно есть поменьше");
        }
    }
}
