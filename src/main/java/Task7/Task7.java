package Task7;

import java.util.Arrays;
import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        String[] familyOwner = new String[n];
        String[] nicknameDog = new String[n];
        double[][] scoresDog = new double[n][3];
        for (int i = 0; i < n; i++) {
            familyOwner[i] = scanner.next();
        }
        for (int i = 0; i < n; i++) {
            nicknameDog[i] = scanner.next();
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < 3; j++) {
                scoresDog[i][j] = scanner.nextDouble();
            }
        }
        double[][] raiting = new double[4][2];
        for (int i = 0; i < n; i++) {
            raiting[i][0] = 0;
            for (int j = 0; j < 3; j++) {
                raiting[i][0] = raiting[i][0] + scoresDog[i][j];
            }
            raiting[i][0] = raiting[i][0] / 3;
            raiting[i][1] = i;
        }
        for (int i = 0; i < n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                if (raiting[i][0] < raiting[j][0]) {
                    double temp = raiting[i][0];
                    raiting[i][0] = raiting[j][0];
                    raiting[j][0] = temp;
                    temp = raiting[i][1];
                    raiting[i][1] = raiting[j][1];
                    raiting[j][1] = temp;
                }
            }
        }
        for (int i = 0; i < 3; i++) {
            System.out.print(familyOwner[(int) raiting[i][1]] + ": ");
            System.out.print(nicknameDog[(int) raiting[i][1]] + ", ");
            System.out.println((int) (raiting[i][0] * 10) / 10.0);
        }
    }
}
