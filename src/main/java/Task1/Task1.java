package Task1;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();
        int[][] a = new int[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                a[i][j] = scanner.nextInt();
            }
        }
        for (int i = 0; i < m; i++) {
            int min = a[i][0];
            for (int j = 1; j < n; j++) {
                if (a[i][j] < min) {
                    min = a[i][j];
                }
            }
            System.out.print(min + " ");
        }
    }
}
